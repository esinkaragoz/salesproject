﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proje
{
    /* enum UrunTipi_e
     {
         KITAP,
         GIDA
     };*/

    public class Urun
    {

        const int kitap_tur = 1;
        const int gida_tur = 2;
        const int tibbi_tur = 3;
        const int diger_tur = 0;

        private String isim;
        private int urunSayisi;          
        private double vergi;

        public double Vergi
        {
            get { return vergi; }
            set { vergi = value; }
        }
        public int UrunSayisi
        {
            get { return urunSayisi; }
            set { urunSayisi = value; }
        }
        public Urun()
        {
            
        }
        private int sayi;
        public int Sayi
        {
            get { return sayi; }
            set { sayi = value; }
        }
        public String Isim
        {
            get { return isim; }
            set { isim = value; }
        }
        private double fiyat;

        public double Fiyat
        {
            get { return fiyat; }
            set { fiyat = value; }
        }
        private int tur;

        internal int Tur
        {
            get { return tur; }
            set { tur = value; }
        }
        private double KDVOrani;

        public double KDVOrani1
        {
            get { return KDVOrani; }
            set { KDVOrani = value; }
        }
        private bool ithal;

        public bool Ithal
        {
            get { return ithal; }
            set { ithal = value; }
        }

        public void yazdir()
        {
            Console.WriteLine("isim:{0}", isim);
            Console.WriteLine("fiyat:{0}", fiyat);
            Console.WriteLine("KDV:{0}", KDVOrani);
            Console.WriteLine("ithal:{0}", ithal);
        }

        public void urunSay()
        {
            Console.WriteLine("Urunler:");
        }

        public void vergiHesapla()
        {
            vergi = Math.Round(UrunSayisi*fiyat * KDVOrani1,1);
        }

        public double fiyatHesapla()
        {
            vergiHesapla();
            fiyat = UrunSayisi*(fiyat) + vergi;      
            return fiyat;
        }
    }
}
