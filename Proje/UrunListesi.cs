using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proje
{
    public class UrunListesi
    {
        List<Urun> urunler;
        double toplamVergi;
        double toplamFiyat;

        public UrunListesi()
        {
            urunler = new List<Urun>();
        }
        public void urunEkle(Urun urun)
        {
            urunler.Add(urun);
        }
        bool say;
        public void urunleriGoster()
        {
            Console.WriteLine("Sonu�:");
            for (int i = 0; i < urunler.Count; i++)
            {                
                        urunuYazd�r(urunler[i]);             
            }
            Console.WriteLine("Sales Taxes:{0:0.00}", this.toplamVergi);
            Console.WriteLine("Total:{0:0.00}", this.toplamFiyat);
        }
        readonly string book = "Book";
        readonly string baglac = "at";
        readonly string []tanimlar = {"Book","CD","packet of chips"};
        void urunuYazd�r(Urun urun)
        {

         if(urun.Ithal)
             Console.WriteLine("{0} imported {1}: {2:0.00}", urun.UrunSayisi ,urun.Isim/*tanimlar[urun.Tur]*/,urun.Fiyat);   
         else
             Console.WriteLine("{0} {1}: {2}", urun.UrunSayisi, urun.Isim/*tanimlar[urun.Tur]*/, urun.Fiyat);       
        }
        
        public int UrunSayisi()
        {
            return urunler.Count;
        }

        public void toplamKDVHesapla()
        {
            double vergi = 0.0;
            for(int i = 0; i<urunler.Count;i++)
            {
                urunler[i].vergiHesapla();

                this.toplamVergi += urunler[i].Vergi;
            }
        }

        public void toplamFiyatHesapla()
        {
            for (int i = 0; i < urunler.Count; i++)
            {
                this.toplamFiyat += urunler[i].fiyatHesapla();    
            }
        }

    }
}
