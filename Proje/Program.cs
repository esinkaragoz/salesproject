﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proje
{
    class Program
    {   
        public const string aranan = "at";

        const string ithal = "imported";  
        
        private static Urun urunGir(string key)
        {
            Urun urun = null;
            double urunFiyati;
            int urunTipi;
          
            string[] uruntur = { "book", "chips", "tablets", "chocolates", "oil", "CD", "transformer", "perfume", "whiskey" }; 
            
            urun = new Urun();

          
            char[] bosluk = { ' ' };

            string[] kelimeler = key.Split(bosluk);


            // entera basılmadı. 
            int urunSayisi = 0;
            bool sayimi = int.TryParse(kelimeler[0], out urunSayisi);
            if (sayimi)
            {
                urun.UrunSayisi = urunSayisi;
                for (int a = 0; a < kelimeler.Length; a++)
                {
                    if (kelimeler[a] == aranan)         // at bulundu.
                    {
                        for (int b = 1; b < a; b++)
                        {
                            urun.Isim += ' ' + kelimeler[b];
                        }

                    }
                    if (kelimeler[a] == ithal)      //
                    {
                        urun.Ithal = true;
              
                    }
                    else
                    {
                        urun.Ithal = false;
                    }

                    
                    for (int c = 0; c < uruntur.Length; c++)
                    {
                        if (kelimeler[a] == uruntur[c] )
                        {
                           if(c < 4)
                           {
                                urun.KDVOrani1 = 0;
                           }
                           else
                           {
                               urun.KDVOrani1 = 0.1;
                           }
                            break;
                        }
                    }
                }
                

                if (urun.Isim.Contains(ithal))
                {
                 
                    urun.Isim = urun.Isim.Remove(urun.Isim.IndexOf(ithal), ithal.Length);
                    urun.Ithal = true;
                    urun.KDVOrani1 += 0.05;
                }
            }
            else
            {
                //Hata ver
                Console.WriteLine("HATALI GIRIS!");
                Console.ReadLine();
                Environment.Exit(0);
                return null;
            }
            urun.Fiyat = Double.Parse(kelimeler[kelimeler.Length - 1].Replace('.', ','));
            //urun.yazdir();
            return urun;
        }
        static void Main(string[] args)
        {
            UrunListesi liste = new UrunListesi();

            string key = "a";
            int secim = 0;
            Urun urun = null;
           while(true)
            {

                key = Console.ReadLine();
                if ((key.Length == 0))
                    break;
                urun = urunGir(key);
                if (urun != null)
                {
                    liste.urunEkle(urun);
                }

            };
            liste.toplamKDVHesapla();
            liste.toplamFiyatHesapla();
            liste.urunleriGoster();
            Console.ReadLine();
        }


    }
}
